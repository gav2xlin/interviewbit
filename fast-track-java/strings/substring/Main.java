import java.lang.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUTS
        // E.g. 'Scanner' for input & 'System.out' for output
        Scanner in = new Scanner(System.in);
        String A = in.nextLine();
        int L = in.nextInt(), R = in.nextInt();
        in.close();

        System.out.println(A.substring(L, R + 1));
    }
}
