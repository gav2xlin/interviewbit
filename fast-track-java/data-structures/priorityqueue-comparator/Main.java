import java.lang.*;
import java.util.*;

class Ordering implements Comparator<Integer> {
    public int compare(Integer x, Integer y) {
        return y - x;
    }
}

public class Main {
    public static void main(String[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUTS
        // E.g. 'Scanner' for input & 'System.out' for output
        PriorityQueue<Integer> q = new PriorityQueue<>(new Ordering());
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < n; ++i) {
            int x = scanner.nextInt(), y = scanner.nextInt();
            if (x == 1) {
                q.offer(y);
            } else if (x == 2) {
                if (q.peek() != null) {
                    System.out.println(q.peek());
                } else {
                    System.out.println(-1);
                }
            } else if (x == 3) {
                q.poll();
            }
        }
    }
}
