import java.lang.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUTS
        // E.g. 'Scanner' for input & 'System.out' for output
        Deque<Integer> q = new ArrayDeque<>();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < n; ++i) {
            int x = scanner.nextInt(), y = scanner.nextInt();
            if (x == 1) {
                q.addFirst(y);
            } if (x == 2) {
                q.addLast(y);
            } else if (x == 3) {
                if (q.peekLast() != null) {
                    System.out.println(q.peekLast());
                } else {
                    System.out.println(-1);
                }
            } else if (x == 4) {
                if (q.peekFirst() != null) {
                    System.out.println(q.peekFirst());
                } else {
                    System.out.println(-1);
                }
            } else if (x == 5) {
                q.pollLast();
            } else if (x == 6) {
                q.pollFirst();
            }
        }
    }
}
