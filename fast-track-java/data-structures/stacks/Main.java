import java.lang.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUTS
        // E.g. 'Scanner' for input & 'System.out' for output
        Scanner scanner = new Scanner(System.in);
        Stack<Character> stack = new Stack<>();

        int n = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < n; ++i) {
            String s = scanner.nextLine();

            stack.clear();
            int balanced = 1;
            for (char c : s.toCharArray()) {
                if (c == '(') {
                    stack.push(c);
                } else if (c == ')') {
                    if (stack.empty()) {
                        balanced = 0;
                        break;
                    }
                    stack.pop();
                }
            }
            if (!stack.empty()) {
                balanced = 0;
            }

            System.out.println(balanced);
        }
    }
}
