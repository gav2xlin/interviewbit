class InterviewBit
{ 
    public static void main (String[] args)  
    { 
          
        // array of size 10. 
        int[] arr = new int[10]; 
          
        try
        { 
            int i = arr[11]; 
            System.out.print("A" + " "); 
        } 
          
        catch(ArrayIndexOutOfBoundsException ex) 
        { 
            System.out.print("B" + " "); 
        } 
          
        finally
        { 
            System.out.print("C" + " "); 
        } 
          
        // rest program will be executed 
        System.out.println("D"); 
    } 
}
