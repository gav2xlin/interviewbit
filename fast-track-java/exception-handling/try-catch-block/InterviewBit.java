class InterviewBit
{ 
    public static void main (String[] args)  
    { 
          
        // array of size 4. 
        int[] arr = new int[10]; 
        try
        { 
            int i = arr[14]; 
            System.out.print("A" + " "); 
        } 
        catch(ArrayIndexOutOfBoundsException ex) 
        { 
            System.out.print("B"+ " "); 
        } 
        System.out.println("C"); 
    } 
}
