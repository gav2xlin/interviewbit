import java.lang.*;
import java.util.*;
import java.math.BigDecimal;

class Ordering implements Comparator<String> {
    @Override
    public int compare(String s1, String s2) {
        return new BigDecimal(s2).compareTo(new BigDecimal(s1));
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] s = new String[n];
        for(int i = 0; i < n; i++) {
            s[i] = sc.next();
        }
      	sc.close();

        //Write your code here
        Arrays.sort(s, new Ordering());

        //Output
        for(int i = 0; i < n; i++)
        {
            System.out.println(s[i]);
        }
    }
}
