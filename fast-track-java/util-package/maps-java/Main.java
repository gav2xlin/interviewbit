import java.lang.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUTS
        // E.g. 'Scanner' for input & 'System.out' for output
        Map<String, Integer> students = new HashMap<>();
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        in.nextLine();
        for (int i = 0; i < a; ++i) {
            String name = in.nextLine();
            int mark = in.nextInt();
            in.nextLine();
            students.put(name, mark);
        }
        int q = in.nextInt();
        in.nextLine();
        for (int i = 0; i < q; ++i) {
            String name = in.nextLine();
            if (students.containsKey(name)) {
                System.out.println(students.get(name));
            } else {
                System.out.println("Not Found");
            }
        }
        in.close();
    }
}
