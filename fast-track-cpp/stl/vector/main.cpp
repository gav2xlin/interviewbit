#include<iostream>
#include <vector>
#include<algorithm>
using namespace std;

int main()  {
    // YOUR CODE GOES HERE
    // Please take input and print output to standard input/output (stdin/stdout)
    // E.g. 'cin' for input & 'cout' for output
    int n;
    cin >> n;

    vector<int> v(n);
    for (int& d : v) {
        cin >> d;
    }

    int x;
    cin >> x;
    v.erase(v.begin() + x);

    sort(v.begin(), v.end());
    for (int& d : v) {
        cout << d << endl;
    }

    return 0;
}
