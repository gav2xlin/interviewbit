#include<iostream>
#include<vector>
#include<algorithm>
#include<iterator>
using namespace std;

int main()  {
    // YOUR CODE GOES HERE
    // Please take input and print output to standard input/output (stdin/stdout)
    // E.g. 'cin' for input & 'cout' for output
    int n;
    cin >> n;

    vector<int> vec(n);
    for (int& v : vec) {
        cin >> v;
    }

    int q;
    cin >> q;

    while (q-- > 0) {
        int x;
        cin >> x;

        vector<int>::iterator lower = lower_bound(vec.begin(), vec.end(), x);
        vector<int>::iterator upper = upper_bound(vec.begin(), vec.end(), x);

        int index = min(lower - vec.begin(), upper - vec.begin());
        cout << index << endl;
    }

    return 0;
}
