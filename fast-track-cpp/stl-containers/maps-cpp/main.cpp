#include<iostream>
#include<map>
using namespace std;

int main()  {
    // YOUR CODE GOES HERE
    // Please take input and print output to standard input/output (stdin/stdout)
    // E.g. 'cin' for input & 'cout' for output
    int q, x, y;
    cin >> q;

    map<int, int> m;
    for (int i = 0; i < q; ++i) {
        int r;
        cin >> r;
        if (r == 1) {
            cin >> x >> y;
            m[x] += y;
        } else if (r == 2) {
            cin >> x;
            if (m.count(x)) {
                m.erase(x);
            }
        } else if (r == 3) {
            cin >> x;
            cout << m[x] << endl;
        }
    }
    
    return 0;
}
