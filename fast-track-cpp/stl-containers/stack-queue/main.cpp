/*
#include<iostream>
#include<bits/stdc++.h>
using namespace std;
*/

// Complete the given function
vector<int> solve(vector<int> &A, int B) {
    vector<int> res;
    res.reserve(A.size());

    deque<int> dq;
    for (int i = 0; i < A.size(); ++i) {
        if (!dq.empty() && dq.front() == i - B) {
            dq.pop_front();
        }

        while (!dq.empty() && A[dq.back()] < A[i]) {
            dq.pop_back();
        }

        dq.push_back(i);

        if (i >= B - 1) {
            res.push_back(A[dq.front()]);
        }
    }
    return res;
}
/*
int main()  {
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        vector<int> A(n);
        for(int i = 0; i < n; i++){
            cin>>A[i];
        }
        int B;
        cin>>B;
        vector<int> ans = solve(A, B);
        for(int i = 0; i < ans.size(); i++){
            cout<<ans[i]<<" ";
        }
        cout<<endl;
    }
    return 0;
}
*/
