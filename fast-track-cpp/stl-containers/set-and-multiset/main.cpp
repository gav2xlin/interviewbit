#include<iostream>
#include<set>
#include<iterator>
using namespace std;

int main()  {
    // YOUR CODE GOES HERE
    // Please take input and print output to standard input/output (stdin/stdout)
    // E.g. 'cin' for input & 'cout' for output
    int q;
    cin >> q;

    set<int> s;
    while (q-- > 0) {
        int y, x;
        cin >> y >> x;

        if (y == 1) {
            s.insert(x);
        } else if (y == 2) {
            if (s.find(x) != s.end()) {
                s.erase(x);
            }
        } else if (y == 3) {
            cout << (s.count(x) ? "Yes" : "No") << endl;
        }
    }
    
    for (int v : s) {
        cout << v << endl;
    }

    return 0;
}
