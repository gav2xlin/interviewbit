#include<iostream>
#include<string>
using namespace std;

// Create Structure named 'student'
struct student {
    string name;
    int rollno, marks;
};

int main()  {
    // Your code goes here
    student s1{"Robin", 11, 86};
    
    // Don't change the below code
    cout<<s1.name<<endl;
    cout<<s1.rollno<<endl;
    cout<<s1.marks<<endl;
    return 0;
}
