using System;
using System.IO;
using System.Collections.Generic;

class MAIN  {
    public static void Main(string[] args) {
        // Declare a linked list of integer type
        LinkedList<int> list = new LinkedList<int>();

        // Add all numbers from 1 to 10 to the beginning
        for(int i = 1; i <= 10; i++){
            list.AddFirst(i);
        }

        // Remove 3 elements from the beginning
        for(int i = 1; i <= 3; i++){
            list.RemoveFirst();
        }

        // Print the elements of the list separated by a space
        foreach (int x in list){
            Console.Write(x + " ");
        }
        Console.WriteLine();

        // Remove 3 elements from the end
        for(int i = 1; i <= 3; i++){
            list.RemoveLast();
        }

        // Print the elements of the list separated by a space
        foreach (int x in list){
            Console.Write(x + " ");
        }
        Console.WriteLine();

        // Find how many multiples of 3 are present in the list
        int count = 0;
        foreach (int x in list){
            if(x % 3 == 0){
                count++;
            }
        }
        Console.WriteLine(count);

        // Find the length of the list
        Console.WriteLine(list.Count); 

        // Print the last element in the list
        Console.WriteLine(list.Last.Value);
    }
}
