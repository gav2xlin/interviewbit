using System;
using System.IO;
using System.Collections.Generic;

class MAIN  {
    public static void Main(string[] args) {
        // Declare a sorted set of integer type
        SortedSet < int > set = new SortedSet < int > ();

        // Add all even numbers from 1 to 15
        for (int i = 2; i <= 15; i += 2) {
            set.Add(i);
        }

        // Remove the largest element
        set.Remove(set.Max);

        // Remove the 2nd smallest element
        int min = set.Min;
        set.Remove(set.Min);
        set.Remove(set.Min);
        set.Add(min);

        // Insert first 5 odd numbers
        for (int i = 1; i < 10; i += 2) {
            set.Add(i);
        }

        // Print the elements of the set separated by a space
        foreach(int x in set) {
            Console.Write(x + " ");
        }
        Console.WriteLine();

        // Print the size of the set
        Console.WriteLine(set.Count);
    }
}
