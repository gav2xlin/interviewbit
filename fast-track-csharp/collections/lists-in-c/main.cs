using System;
using System.IO;
using System.Collections.Generic;

class MAIN {
    public static void Main(string[] args) {
        // Declare a list of integer type
        List < int > l = new List < int > ();

        // Add all numbers from 1 to 10 to the end
        for (int i = 1; i <= 10; i++) {
            l.Add(i);
        }

        // Remove element from the beginning
        l.RemoveAt(0);

        // Remove 8 from the list
        l.Remove(8);

        // Insert 4 at the 1st index
        l.Insert(1, 4);

        // Print the elements of the list separated by a space
        for (int i = 0; i < l.Count; i++) {
            Console.Write(l[i] + " ");
        }
        Console.WriteLine();

        // Reverse the list
        l.Reverse();

        // Print the elements of the list separated by a space
        for (int i = 0; i < l.Count; i++) {
            Console.Write(l[i] + " ");
        }
        Console.WriteLine();

        // Sort the list
        l.Sort();

        // Print the elements of the list separated by a space
        for (int i = 0; i < l.Count; i++) {
            Console.Write(l[i] + " ");
        }
        Console.WriteLine();
    }
}
