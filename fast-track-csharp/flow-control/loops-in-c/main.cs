using System;
using System.IO;

class MAIN  {
    public static void Main(string[] args) {
        int num1 = Convert.ToInt32(Console.ReadLine());
        
        // Find the factorial of num1 and print it in a separate line
        long f = 1;
        for (int i = 1; i <= num1; ++i) {
            f *= i;
        }
        Console.WriteLine(f);
  
        int num2 = Convert.ToInt32(Console.ReadLine());
        
        // Find the highest power of 2 that divides num2
        // and print it in a separate line
        int p = 0;
        while(num2 % 2 == 0) {
            num2 /= 2;
            ++p;
        }
        Console.WriteLine(p);
    }
}
