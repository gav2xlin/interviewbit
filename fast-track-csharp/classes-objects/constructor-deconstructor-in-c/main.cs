public class Student  {
    private string name;
    private int age, rollno;
   
    public Student(string n, int a, int r) {  
        name = n;
        age = a;
        rollno = r;
    }

    ~Student()  {  
        Console.WriteLine(name + " " + age + " " + rollno);
    }
}

