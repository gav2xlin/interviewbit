/*
using System;  
public class Employee  {  
    public int id;   
    public String name;  
    public float salary;  
    public Employee(int id, String name,float salary)  {  
        this.id = id;  
        this.name = name;  
        this.salary = salary;  
    }  
    public void display()  {  
        Console.WriteLine(id + " " + name+" "+salary);  
    }  
}  
class TestEmployee{  
    public static void Main(string[] args)  {  
        Employee e1 = new Employee(101, "Sonoo", 890000f);  
        Employee e2 = new Employee(102, "Mahesh", 490000f);  
        e1.display();  
        e2.display();  

    }  
}
*/

/*
using System;  
public class Account  {  
    public int accno;   
    public String name;  
    public static float rateOfInterest=8.8f;  
    public Account(int accno, String name)  {  
        this.accno = accno;  
        this.name = name;  
    }  

    public void display()  {  
        Console.WriteLine(accno + " " + name + " " + rateOfInterest);  
    }  
}  
class TestAccount{  
    public static void Main(string[] args)  {  
        Account a1 = new Account(101, "Sonoo");  
        Account a2 = new Account(102, "Mahesh");  
        a1.display();  
        a2.display();  

    }  
}
*/
