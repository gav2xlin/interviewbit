using System;
using System.IO;

class MAIN  {
    public static void Main(string[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUT
        // E.g. 'StreamReader' for input & 'StreamWriter' for output
        int i = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine(i);

        long l = Convert.ToInt64(Console.ReadLine());
        Console.WriteLine(l);

        char c = Convert.ToChar(Console.ReadLine());
        Console.WriteLine(c);

        float f = Convert.ToSingle(Console.ReadLine());
        Console.WriteLine(f);

        double d = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine(d);
    }
}
