using System;
using System.IO;
using System.Collections.Generic;
class Cube{
    public int length, breadth, height;
    public Cube(int length, int breadth, int height){
        this.length = length;
        this.breadth = breadth;
        this.height = height;
    }
}

// Define CompareDimension here
class CompareDimension : Comparer < Cube > {
    public override int Compare(Cube A, Cube B) {
        if (A.length != B.length) {
            return A.length - B.length;
        } else if (A.breadth != B.breadth){
            return B.breadth - A.breadth;
        } else {
            return A.height - B.height;
        }
    }
}

// Define CompareVolume here
class CompareVolume : Comparer < Cube > {
    public override int Compare(Cube A, Cube B) {
        int volA = A.length * A.breadth * A.height;
        int volB = B.length * B.breadth * B.height;
        return volA - volB;
    }
}

class MAIN {
    public static void Main(string[] args) {
        List<Cube>l1 = new List<Cube>();
        int n = Convert.ToInt32(Console.ReadLine());
        for(int i = 0;i<n;i++){
            String[] temp = Console.ReadLine().Split(' ');
            int l = Convert.ToInt32(temp[0]);
            int b = Convert.ToInt32(temp[1]);
            int h = Convert.ToInt32(temp[2]);
            l1.Add(new Cube(l, b, h));
        }        
        l1.Sort(new CompareDimension());
        for(int i=0;i<n;i++){
            Console.WriteLine(l1[i].length+" "+l1[i].breadth+" "+l1[i].height);
        }
        l1.Sort(new CompareVolume());
        for(int i=0;i<n;i++){
            Console.WriteLine(l1[i].length+" "+l1[i].breadth+" "+l1[i].height);
        }
    }
}
