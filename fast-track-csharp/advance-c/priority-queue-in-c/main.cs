using System;
using System.IO;
using System.Collections.Generic;

class MAIN {
    public static void Main(string[] args) {
        int n = Convert.ToInt32(Console.ReadLine());
        List<int> l1 = new List<int>();
        String[] temp = Console.ReadLine().Split(' ');
        for(int i = 0;i<n;i++){
            int item = Convert.ToInt32(temp[i]);
            l1.Add(item);
        } 

        // Define priority queue (min - heap)
        var queue = new PriorityQueue<int>();
        for (int i = 0; i < n; i++) {
            queue.Enqueue(l1[i], l1[i]);
        }
        
        int x = Convert.ToInt32(Console.ReadLine());
        // Remove X smallest elements from the array
        for (int i = 0; i < x; i++) {
            queue.Dequeue();
        }

        int y = Convert.ToInt32(Console.ReadLine());
        // Add first y natural numbers to the heap
        for(int i=1;i<=y;i++){
            queue.Enqueue(i, i);
        }
        
        // Print the difference between the max and min element in heap
        int min = queue.Dequeue();
        int max = min;
        while(queue.Count > 0){
            max = queue.Dequeue();
        }
        Console.WriteLine(max - min);
    }
}
