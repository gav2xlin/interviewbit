using System;
using System.IO;

class MAIN  {
    public static void Main(string[] args) {
        // YOUR CODE GOES HERE
        // Please take input and print output to standard input/output (stdin/stdout)
        // DO NOT USE ARGUMENTS FOR INPUT
        // E.g. 'StreamReader' for input & 'StreamWriter' for output
        int n = Convert.ToInt32(Console.ReadLine());
        int[] arr = new int[n];

        for (int i = 0; i < n; ++i) {
            arr[i] = Convert.ToInt32(Console.ReadLine());
        }

        for (int i = n - 1; i >= 0; --i) {
            Console.WriteLine(arr[i]);
        }
    }
}
