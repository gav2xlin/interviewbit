//Complete the below function and print the result
// arr -> 2D Int Array
static void column_sum(int[,] arr){
    int n = arr.GetLength(0), m = arr.GetLength(1);

	for (int i = 0; i < m; ++i) {
        int sum = 0;
		for (int j = 0; j < n; ++j) {
			sum = sum + arr[j, i];
		}

		Console.Write(sum + " ");
    }
}
